﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBClasses.Entities;
using DBClasses.Configurations;

namespace DBClasses.Contexts
{
    public class LessonContext : DbContext
    {
        public DbSet<LessonEntity> Lessons { get; set; }

        public LessonContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["GymDB"].ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LessonEntity>().
                HasMany(l => l.Users)
                .WithMany(c => c.Lessons)
                .UsingEntity(cl => cl.ToTable("UserLessons"));
            modelBuilder.ApplyConfiguration(new LessonConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
