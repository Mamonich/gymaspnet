﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using DBClasses.Entities;
using DBClasses.Configurations;
using System.Collections.Generic;

namespace DBClasses.Contexts
{
    public class GymContext : DbContext
    {
        public DbSet<GymEntity> Gyms{ get; set; }

        public GymContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["GymDB"].ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<UserEntity>()
                .HasMany(p => p.Lessons)
                .WithMany(p => p.Users)
                .UsingEntity(j => j.ToTable("UserLessons"));
            modelBuilder.ApplyConfiguration(new GymConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
