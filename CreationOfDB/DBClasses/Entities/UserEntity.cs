﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBClasses.Entities
{
    [Table(name: "Users")]
    public class UserEntity
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }

        // relations data
        //
        public ICollection<LessonEntity> Lessons { get; set; }
        public AdministratorEntity Administrator{ get; set; }
    }
}
