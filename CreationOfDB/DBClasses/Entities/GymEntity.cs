﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBClasses.Entities
{
    [Table(name:"Gyms")]
    public class GymEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }

        // relations data
        //
        public ICollection<ExerciseHallEntity> ExerciseHalls { get; set; }
    }
}
