﻿using DBClasses.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBClasses.Configurations
{
    internal class GymConfiguration : IEntityTypeConfiguration<GymEntity>
    {
        public void Configure(EntityTypeBuilder<GymEntity> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.Property(_ => _.Title);
            builder.Property(_ => _.Address);
            builder.HasMany(g => g.ExerciseHalls)
                .WithOne(h => h.Gym)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.Cascade);
           
        }
    }
}