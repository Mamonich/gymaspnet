﻿using DBClasses.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBClasses.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.Property(_ => _.Name);
            builder.Property(_ => _.Surname);
            builder.Property(_ => _.PhoneNumber);
            builder.Property(_ => _.Email);
            builder.Property(_ => _.Username);
            builder.Property(_ => _.Password);
            builder.Property(_ => _.Gender);
            builder.Property(_ => _.BirthDate);

        }
    }
}