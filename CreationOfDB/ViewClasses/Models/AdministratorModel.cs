﻿using ViewClasses.Enums;
using System;

namespace ViewClasses.Models
{
    public class AdministratorModel
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public string BirthDate { get; set; }
        public string AdminType { get; set; }
        public string WorkBeginTime { get; set; }
        public string WorkEndTime { get; set; }
    }
}
