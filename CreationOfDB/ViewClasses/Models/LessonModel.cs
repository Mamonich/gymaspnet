﻿using ViewClasses.Enums;

namespace ViewClasses.Models
{
    public class LessonModel
    { 
        public int Id{get;set;}
        public string LessonBeginTime{get;set;}
        public string LessonEndTime{get;set;}
        public string LessonType{get;set;}
        public string TrainerName{get;set;}
        public int ClientCount { get; set; }
        public int MaxClientSize { get; set; }
        public int TrainerId { get; set; }
    }                                                     
}