﻿namespace ViewClasses.Enums
{
    public enum TrainerSpecialisation
    {
        YOGA,
        FITNESS,
        AEROBICS,
        DANCE,
        KARATE,

    }
}