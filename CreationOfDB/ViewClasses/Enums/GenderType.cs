﻿namespace ViewClasses.Enums
{
    public enum GenderType
    {
        MALE,
        FEMALE
    }
}