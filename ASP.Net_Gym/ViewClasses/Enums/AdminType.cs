﻿namespace ViewClasses.Enums

{
    public enum AdminType
    {
        NO_ADMIN,
        MANAGER,
        MAINMANAGER
    }
}