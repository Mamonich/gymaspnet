﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewClasses.Models
{
    public class UserLessonsModel
    {
        public int UserId{ get; set; }
        public int LessonId{ get; set; }
        
    }
}
