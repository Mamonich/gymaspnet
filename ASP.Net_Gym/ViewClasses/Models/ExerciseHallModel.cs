﻿using ViewClasses.Enums;

namespace ViewClasses.Models
{
    public class ExerciseHallModel
    {
        public int Id { get; set; }
        public string TrainerSpecialisation { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int AdministratorId { get; set; }
        public int GymId { get; set; }
    }                                                     
}