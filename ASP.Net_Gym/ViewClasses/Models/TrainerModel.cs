﻿using ViewClasses.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewClasses.Models
{
    public class TrainerModel
    {
        public int Id{ get; set; }
        public string Surname{ get; set; }
        public string Name{ get; set; }
        public string PhoneNumber{ get; set; }
        public string Email{ get; set; }
        public string Username{ get; set; }
        public string Password{ get; set; }
        public string Gender{ get; set; }
        public string BirthDate{ get; set; }
        public string TrainerType { get; set; }
        public string TrainerSpecialisation { get; set; }
        public string WorkBeginTime { get; set; }
        public string WorkEndTime { get; set; }
        public string TrainerSchedule { get; set; }
        public int UserId { get; set; }
        public int ExerciseHallId { get; set; }
    }
}
