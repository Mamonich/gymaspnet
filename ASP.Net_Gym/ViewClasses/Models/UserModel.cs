﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViewClasses.Models
{
    public class UserModel
    {
        public int Id{ get; set; }
        public string Surname{ get; set; }
        public string Name{ get; set; }
        public string PhoneNumber{ get; set; }
        public string Email{ get; set; }
        public string Username{ get; set; }
        public string Password{ get; set; }
        public string Gender{ get; set; }
        //public string BirthDate{ get; set; }
        public string BirthDateDay { get; set; }
        public string BirthDateMonth { get; set; }
        public string BirthDateYear { get; set; }
        public int IsAdmin { get; set; } // 0 - no; 1 - admin; 2 - mainAdmin
    }
}
