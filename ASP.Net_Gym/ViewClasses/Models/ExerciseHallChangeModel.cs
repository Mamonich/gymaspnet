﻿using System.Collections.Generic;
using ViewClasses.Enums;

namespace ViewClasses.Models
{
    public class ExerciseHallChangeModel
    {
        public ExerciseHallModel ExerciseHallModel { get; set; }
        public List<string> TrainerSpecialisations { get; set; }
    }
}