﻿using DBClasses.Contexts;
using DBClasses.Entities;
using ViewClasses.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewClasses.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ASP.Net_Gym.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LessonController : ControllerBase
    {
        private List<LessonModel> Lessons = new List<LessonModel>();
        private LessonContext lessonContext = new LessonContext();
        private UserContext userContext= new UserContext();
        private TrainerContext trainerContext= new TrainerContext();

        public LessonController()
        {           
            SetDBDataToList();
            if (Lessons.Count == 0)
            {
                List<TrainerEntity> trainers = trainerContext.Trainers.ToList();
                for (int i = 0; i < 5; i++)
                {
                    Random r = new Random();
                    List<int> rndDaysOfWeek = new List<int>();
                    rndDaysOfWeek.Add(r.Next(6));
                    int firstRandomNumber = rndDaysOfWeek[0];
                    while (true)
                    {
                        int secondRandomNumber = r.Next(6);
                        if (!firstRandomNumber.Equals(secondRandomNumber))
                        {
                            rndDaysOfWeek.Add(secondRandomNumber);
                            break;
                        }
                    }
                    string lessonScheduleString = firstRandomNumber + ", " + rndDaysOfWeek[1];

                    lessonContext.Add(new LessonEntity
                    {                        
                        LessonBeginTime = new TimeSpan(10 + i, 0, 0),
                        LessonEndTime = new TimeSpan(15 + i, 0, 0),
                        LessonType = LessonType.PUBLIC,
                        LessonSchedule = lessonScheduleString,
                        MaxClientSize = i + 5,
                        TrainerId = trainers[i].Id
                    });
                }
                lessonContext.SaveChanges();
                SetDBDataToList();
            }
        }

        private void SetDBDataToList()
        {
            foreach (LessonEntity lessonEntity in lessonContext.Lessons)
            {
                TrainerEntity trainerEntity = trainerContext.Trainers.Find(lessonEntity.TrainerId);
                UserEntity userEntity = userContext.Users.Find(trainerEntity.UserId);

                string lessonModelSchedule = "";
                foreach(string stringDay in lessonEntity.LessonSchedule.Split(","))
                {
                    ViewClasses.Enums.DayOfWeek dayOfWeek = Enum.Parse<ViewClasses.Enums.DayOfWeek>(stringDay);
                    lessonModelSchedule += dayOfWeek.ToString() + ",";
                };

                lessonModelSchedule = lessonModelSchedule.Remove(lessonModelSchedule.Length - 1, 1); // removing last ","
                Lessons.Add(new LessonModel
                {
                    Id = lessonEntity.Id,
                    LessonBeginTime = lessonEntity.LessonBeginTime.ToString(),
                    LessonEndTime = lessonEntity.LessonEndTime.ToString(),
                    LessonType = lessonEntity.LessonType.ToString(),
                    LessonSchedule = lessonModelSchedule,
                    MaxClientSize = lessonEntity.MaxClientSize,
                    TrainerName = userEntity.Surname + " " + userEntity.Name,
                    ExerciseHallId = trainerEntity.ExerciseHallId,
                    TrainerId = trainerEntity.Id
                });
            }
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public ActionResult Index()
        {
            return Ok(Lessons);
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(Lessons.Find(l => l.Id == id));
        }

        // POST api/<ValuesController>
        [HttpPost]
        public ActionResult Post([FromBody] LessonModel lessonModel)
        {
            if (lessonModel.TrainerId.Equals(null))
            {
                return BadRequest();
            }

            lessonContext.Add(new LessonEntity
            {
                LessonBeginTime = TimeSpan.Parse(lessonModel.LessonBeginTime),
                LessonEndTime = TimeSpan.Parse(lessonModel.LessonEndTime),
                LessonType = Enum.Parse<LessonType>(lessonModel.LessonType),
                LessonSchedule = GetLessonEntitySchedule(lessonModel),
                MaxClientSize = lessonModel.MaxClientSize,
                TrainerId = lessonModel.TrainerId
            });
            lessonContext.SaveChanges();
            return Ok();
        }

        private string GetLessonEntitySchedule(LessonModel lessonModel)
        {
            string lessonEntitySchedule = "";
            foreach (string stringDay in lessonModel.LessonSchedule.Split(","))
            {
                ViewClasses.Enums.DayOfWeek dayOfWeek = Enum.Parse<ViewClasses.Enums.DayOfWeek>(stringDay);
                lessonEntitySchedule += ((int)dayOfWeek) + ",";
            };

            lessonEntitySchedule = lessonEntitySchedule.Remove(lessonEntitySchedule.Length - 1, 1); // removing last ","
            return lessonEntitySchedule;
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] LessonModel lessonModel)
        {
            LessonEntity dbLessonEntity = lessonContext.Lessons.Find(id);
            dbLessonEntity.LessonBeginTime = TimeSpan.Parse(lessonModel.LessonBeginTime);
            dbLessonEntity.LessonEndTime = TimeSpan.Parse(lessonModel.LessonEndTime);
            dbLessonEntity.LessonSchedule = GetLessonEntitySchedule(lessonModel);
            lessonContext.SaveChanges();
            return Ok();
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            lessonContext.Lessons.Remove(lessonContext.Lessons.Find(id));
            lessonContext.SaveChanges();
            return NoContent();
        }
    }
}
