﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewClasses.Enums;

namespace ASP.Net_Gym.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EnumController : ControllerBase
    {
        private List<string> trainerSpecialisations = Enum.GetNames<TrainerSpecialisation>().ToList();

        [HttpGet]
        public ActionResult Index()
        {
            return Ok(trainerSpecialisations);
        }

    }
}
