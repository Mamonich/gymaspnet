﻿using ViewClasses.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DBClasses.Contexts;
using DBClasses.Entities;
using ViewClasses.Enums;

namespace ASP.Net_Gym.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class TrainerController : ControllerBase
    {
        private List<TrainerModel> Trainers = new List<TrainerModel>();
        private TrainerContext trainerContext= new TrainerContext();
        private UserContext userContext = new UserContext();
        private ExerciseHallContext exerciseHallContext= new ExerciseHallContext();

        public TrainerController()
        {
            SetDBDataToList();
            if (Trainers.Count == 0)
            {
                List<UserEntity> users = userContext.Users.ToList();
                for (int i = 0; i < 5; i++)
                {
                    trainerContext.Add(new TrainerEntity
                    {
                        WorkBeginTime = new TimeSpan(10 + i, 0, 0),
                        WorkEndTime = new TimeSpan(15 + i, 0, 0),
                        TrainerType= i == 0? TrainerType.MAINTRAINER : TrainerType.TRAINER,
                        TrainerSpecialisation=  (TrainerSpecialisation)i,
                        UserId = users[i + 5].Id,
                        ExerciseHallId = exerciseHallContext.ExerciseHalls.First().Id // because I would like to give 1 hall all trainers
                    });
                }
                trainerContext.SaveChanges();
                SetDBDataToList();
            }
        }
        private void SetDBDataToList()
        {
            foreach (TrainerEntity trainerEntity in trainerContext.Trainers)
            {
                UserEntity userEntity = userContext.Users.Find(trainerEntity.UserId);

                string trainerModelSchedule = "";
                foreach (string stringDay in trainerEntity.TrainerSchedule.Split(","))
                {
                    ViewClasses.Enums.DayOfWeek dayOfWeek = Enum.Parse<ViewClasses.Enums.DayOfWeek>(stringDay);
                    trainerModelSchedule += dayOfWeek.ToString() + ",";
                };

                trainerModelSchedule = trainerModelSchedule.Remove(trainerModelSchedule.Length - 1, 1); // removing last ","

                Trainers.Add(new TrainerModel
                {
                    Id = trainerEntity.Id,
                    Surname = userEntity.Surname,
                    Name = userEntity.Name,
                    PhoneNumber = userEntity.PhoneNumber,
                    Email = userEntity.Email,
                    Username = userEntity.Username,
                    Password = userEntity.Password,
                    Gender = userEntity.Gender,
                    TrainerType= trainerEntity.TrainerType.ToString(),
                    TrainerSpecialisation=trainerEntity.TrainerSpecialisation.ToString(),
                    BirthDate = userEntity.BirthDate.ToString(),
                    WorkBeginTime = trainerEntity.WorkBeginTime.ToString(),
                    WorkEndTime = trainerEntity.WorkEndTime.ToString(),
                    TrainerSchedule = trainerModelSchedule,
                    ExerciseHallId = trainerEntity.ExerciseHallId,
                    UserId = userEntity.Id
                });
            }
        }

        private string GetTrainerEntitySchedule(TrainerModel trainerModel)
        {
            string trainerEntitySchedule = "";
            foreach (string stringDay in trainerModel.TrainerSchedule.Split(","))
            {
                ViewClasses.Enums.DayOfWeek dayOfWeek = Enum.Parse<ViewClasses.Enums.DayOfWeek>(stringDay);
                trainerEntitySchedule += ((int)dayOfWeek) + ",";
            };

            trainerEntitySchedule = trainerEntitySchedule.Remove(trainerEntitySchedule.Length - 1, 1); // removing last ","
            return trainerEntitySchedule;
        }

        // GET: GymController
        [HttpGet]
        public ActionResult Index()
        {       
            return Ok(Trainers);
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(Trainers.Find(g => g.Id == id));
        }

        // POST api/<ValuesController>
        [HttpPost]
        public ActionResult Post([FromBody] TrainerModel trainerModel)
        {
            trainerContext.Add(new TrainerEntity
            {
                TrainerSpecialisation=Enum.Parse<TrainerSpecialisation>(trainerModel.TrainerSpecialisation),
                TrainerType= Enum.Parse<TrainerType>(trainerModel.TrainerType),
                WorkBeginTime = TimeSpan.Parse(trainerModel.WorkBeginTime),
                TrainerSchedule = GetTrainerEntitySchedule(trainerModel),
                WorkEndTime = TimeSpan.Parse(trainerModel.WorkEndTime),
                UserId = trainerModel.UserId,
                ExerciseHallId = trainerModel.ExerciseHallId
            });
            trainerContext.SaveChanges();
            return Ok();
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] TrainerModel trainerModel)
        {
            TrainerEntity trainerEntity = trainerContext.Trainers.Find(id);
            trainerEntity.TrainerType = Enum.Parse<TrainerType>(trainerModel.TrainerType);
            trainerEntity.TrainerSpecialisation = Enum.Parse<TrainerSpecialisation>(trainerModel.TrainerSpecialisation);
            trainerEntity.WorkBeginTime = TimeSpan.Parse(trainerModel.WorkBeginTime);
            trainerEntity.WorkEndTime = TimeSpan.Parse(trainerModel.WorkEndTime);
            trainerEntity.TrainerSchedule = GetTrainerEntitySchedule(trainerModel);
            trainerEntity.UserId = trainerModel.UserId;
            trainerEntity.ExerciseHallId = trainerModel.ExerciseHallId;
            trainerContext.SaveChanges();
            return Ok();
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            trainerContext.Trainers.Remove(trainerContext.Trainers.Find(id));
            trainerContext.SaveChanges();
            return NoContent();
        }
    }
}
