﻿using DBClasses.Contexts;
using DBClasses.Entities;
using ViewClasses.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ASP.Net_Gym.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class GymController : ControllerBase
    {
        private List<GymModel> Gyms = new List<GymModel>();
        private GymContext gymContext = new GymContext();
        public GymController()
        {

            SetDBDataToList();
            if (Gyms.Count == 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    gymContext.Add(new GymEntity
                    {
                        Title = "Gym" + i,
                        Address = "Address" + i
                    });
                    gymContext.SaveChanges();
                }
                SetDBDataToList();
            }
        }

        private void SetDBDataToList()
        {
            foreach (GymEntity gymEntity in gymContext.Gyms)
            {
                Gyms.Add(new GymModel
                {
                    Id = gymEntity.Id,
                    Title = gymEntity.Title,
                    Address = gymEntity.Address
                });
            }
        }

        // GET: GymController
        [HttpGet]
        public ActionResult Index()
        {            
            return Ok(Gyms);
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(Gyms.Find(g => g.Id == id));
        }

        // POST api/<ValuesController>
        [HttpPost]
        public ActionResult Post([FromBody] GymModel gymModel)
        {
            gymContext.Add(new GymEntity
            {
                Title = gymModel.Title,
                Address = gymModel.Address
            });
            gymContext.SaveChanges();
            return Ok();
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] GymModel gymModel)
        {
            GymEntity gymEntity = gymContext.Gyms.Find(id);
            gymEntity.Title = gymModel.Title;
            gymEntity.Address = gymModel.Address;
            gymContext.SaveChanges();
            return Ok();
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            gymContext.Gyms.Remove(gymContext.Gyms.Find(id));
            gymContext.SaveChanges();
            return NoContent();
        }
    }
}
