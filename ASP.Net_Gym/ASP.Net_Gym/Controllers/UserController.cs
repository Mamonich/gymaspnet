﻿using ViewClasses.Enums;
using ViewClasses.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DBClasses.Contexts;
using DBClasses.Entities;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ASP.Net_Gym.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private List<UserModel> Users = new List<UserModel>();
        private UserContext userContext = new UserContext();
        private AdministratorContext administratorContext= new AdministratorContext();
        private static UserModel activatedUser = new UserModel();

        public UserController()
        {
            SetDBDataToList();
            if (Users.Count == 0)
            {
                for (int i = 0; i < 10; i++)
                {
                    userContext.Add(new UserEntity
                    {
                        Name = "Name" + i,
                        Surname = "Surname" + i,
                        BirthDate = new DateTime(2000 + i, 1 + i, 2 + i),
                        Email = "mail" + i + "@gmail.com",
                        Password = "password" + i,
                        PhoneNumber = "2615631" + i,
                        Username = "UserName" + i,
                        Gender = i % 2 == 0 ? GenderType.MALE.ToString() : GenderType.FEMALE.ToString()
                    });
                }
                userContext.SaveChanges();
                SetDBDataToList();
            }
            
        }
        private void SetDBDataToList()
        {
            foreach(var item in userContext.Users)
            {
                AdministratorEntity administratorEntity = administratorContext.Administrators.FirstOrDefault(a => a.UserId == item.Id);
                
                
                Users.Add(new UserModel
                {
                    Id = item.Id,
                    Name = item.Name,
                    Surname = item.Surname,
                    BirthDateDay = item.BirthDate.Day.ToString(),
                    BirthDateMonth = item.BirthDate.Month.ToString(),
                    BirthDateYear = item.BirthDate.Year.ToString(),
                    Email = item.Email,
                    Password = item.Password,
                    PhoneNumber = item.PhoneNumber,
                    Username = item.Username,
                    Gender = item.Gender,
                    IsAdmin = (int)(administratorEntity is null ? AdminType.NO_ADMIN : administratorEntity.AdminType)
                });
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            return Ok(Users);
        }

        [HttpGet("{id}")]
        public ActionResult GetUser(int id)
        {
           return Ok(Users.Find(u => u.Id == id));
        }

        [HttpGet("activate")]
        public ActionResult GetActivatedUser()
        {
            if (activatedUser.Email is null)
            {
                return NotFound();
            }
            return Ok(activatedUser);
        }
        //Sign in + Registration
        [HttpPost]
        public ActionResult Post([FromBody] UserModel user)
        {
            if (user.Name is null)
            {
                foreach (UserModel item in Users)
                {
                    if (user.Email == item.Email && user.Password == item.Password)
                    {
                        activatedUser = item;
                        return Ok();
                    }
                }
                return NoContent();
            }
            else
            {
                foreach (UserModel item in Users)
                {
                    if (user.Email == item.Email)
                    {
                        return BadRequest("This email has account");
                    }
                }
                string birthDate = getBirthDate(user);
                // add to DB
                userContext.Add(new UserEntity
                {
                    Name = user.Name,
                    Surname = user.Surname,
                    BirthDate = DateTime.Parse(birthDate),
                    Email = user.Email,
                    Password = user.Password,
                    PhoneNumber = user.PhoneNumber,
                    Username = user.Username,
                    Gender = user.Gender,
                });
                userContext.SaveChanges();

                var userEntity = userContext.Users.Find(Users.Count);
                UserModel userModel = new UserModel
                {
                    Id = userEntity.Id,
                    Name = userEntity.Name,
                    Surname = userEntity.Surname,
                    BirthDateDay = userEntity.BirthDate.Day.ToString(),
                    BirthDateMonth = userEntity.BirthDate.Month.ToString(),
                    BirthDateYear = userEntity.BirthDate.Year.ToString(),
                    Email = userEntity.Email,
                    Password = userEntity.Password,
                    PhoneNumber = userEntity.PhoneNumber,
                    Username = userEntity.Username,
                    Gender = userEntity.Gender,
                    IsAdmin = 0
                };
                Users.Add(userModel);
                activatedUser = Users.Find(u => u.Email.Equals(userModel.Email) && u.Password.Equals(userModel.Password));
                return Ok();

            } 
        }

        private static string getBirthDate(UserModel user)
        {
            return user.BirthDateYear + "-" + user.BirthDateMonth + "-" + user.BirthDateDay;
        }       

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] UserModel userModel)
        {
            UserEntity userEntity   = userContext.Users.Find(id);            
            userEntity.Name         = userModel.Name;
            userEntity.Surname      = userModel.Surname;
            userEntity.BirthDate    = DateTime.Parse(getBirthDate(userModel));
            userEntity.Email        = userModel.Email;
            userEntity.Password     = userModel.Password;
            userEntity.PhoneNumber  = userModel.PhoneNumber;
            userEntity.Username     = userModel.Username;
            userEntity.Gender       = userModel.Gender;
            userContext.SaveChanges();
            activatedUser = userModel;
            return Ok();
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            userContext.Users.Remove(userContext.Users.Find(id));
            userContext.SaveChanges();
            return NoContent();
        }
    }
}
