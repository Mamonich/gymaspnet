﻿using DBClasses.Contexts;
using DBClasses.Entities;
using ViewClasses.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewClasses.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ASP.Net_Gym.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserLessonsController : ControllerBase
    {
        private List<UserLessonsModel> UserLessons = new List<UserLessonsModel>();
        private UserLessonsContext userLessonsContext = new UserLessonsContext();

        public UserLessonsController()
        {
            SetDBDataToList();
        }

        private void SetDBDataToList()
        {
            foreach (UserLessonsEntity userLessonEntity in userLessonsContext.UserLessons)
            {
                UserLessons.Add(new UserLessonsModel
                {
                    UserId = userLessonEntity.UserId,
                    LessonId = userLessonEntity.LessonId
                });
            }
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public ActionResult Index()
        {
            return Ok(UserLessons);
        }
        [HttpGet("{id}")]
        public ActionResult Get(int lessonId)
        {
            return Ok(UserLessons.FindAll(_ =>_.LessonId == lessonId));
        }

        [HttpPost]
        public ActionResult Post([FromBody] UserLessonsModel userLesson)
        {
            userLessonsContext.Add(new UserLessonsEntity {UserId = userLesson.UserId, LessonId = userLesson.LessonId });

            userLessonsContext.SaveChanges();
            return Ok();
        }

        [HttpDelete]
        public ActionResult Delete([FromBody] UserLessonsModel userLesson)
        {
            userLessonsContext.UserLessons.Remove(userLessonsContext.UserLessons
                .FirstOrDefault(_ => _.UserId == userLesson.UserId && _.LessonId == userLesson.LessonId));
            userLessonsContext.SaveChanges();
            return Ok();
        }
       
    }
}
