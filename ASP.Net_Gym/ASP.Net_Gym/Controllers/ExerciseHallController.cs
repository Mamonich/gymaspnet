﻿using DBClasses.Contexts;
using DBClasses.Entities;
using ViewClasses.Enums;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewClasses.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ASP.Net_Gym.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ExerciseHallController : ControllerBase
    {
        private List<ExerciseHallModel> ExerciseHalls = new List<ExerciseHallModel>();
        private ExerciseHallContext exerciseHallContext = new ExerciseHallContext();
        private UserContext userContext = new UserContext();
        private TrainerContext trainerContext = new TrainerContext();

        public ExerciseHallController()
        {
            SetDBDataToList();
        }

        private void SetDBDataToList()
        {
            foreach (ExerciseHallEntity exerciseHallEntity in exerciseHallContext.ExerciseHalls)
            {
                try
                {                  
                    ExerciseHalls.Add(new ExerciseHallModel
                    {
                        Id = exerciseHallEntity.Id,
                        Title = exerciseHallEntity.Title,
                        Description = exerciseHallEntity.Description,
                        TrainerSpecialisation = exerciseHallEntity.TrainerSpecialisation.ToString(),
                        AdministratorId = exerciseHallEntity.AdministratorId,
                        GymId = exerciseHallEntity.GymId,
                    });
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public ActionResult Index()
        {
            return Ok(ExerciseHalls);
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(ExerciseHalls.Find(h => h.Id == id));
        }

        // POST api/<ValuesController>
        [HttpPost]
        public ActionResult Post([FromBody] ExerciseHallModel exerciseHallModel)
        {
            Console.WriteLine();
            exerciseHallContext.Add(new ExerciseHallEntity
            {
                Title = exerciseHallModel.Title,
                Description = exerciseHallModel.Description,     
                TrainerSpecialisation = Enum.Parse<TrainerSpecialisation>(exerciseHallModel.TrainerSpecialisation),
                AdministratorId=exerciseHallModel.AdministratorId,
                GymId=exerciseHallModel.GymId,
            });
            exerciseHallContext.SaveChanges();
            return Ok();
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] ExerciseHallModel exerciseHallModel)
        {
            ExerciseHallEntity exerciseHallEntity = exerciseHallContext.ExerciseHalls.Find(id);
            exerciseHallEntity.Title = exerciseHallModel.Title;
            exerciseHallEntity.Description = exerciseHallModel.Description;
            exerciseHallEntity.TrainerSpecialisation = Enum.Parse<TrainerSpecialisation>(exerciseHallModel.TrainerSpecialisation);
            exerciseHallEntity.AdministratorId = exerciseHallModel.AdministratorId;
            exerciseHallEntity.GymId = exerciseHallModel.GymId;
            exerciseHallContext.SaveChanges();
            return Ok();
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            exerciseHallContext.ExerciseHalls.Remove(exerciseHallContext.ExerciseHalls.Find(id));
            exerciseHallContext.SaveChanges();
            return NoContent();
        }
    }
}
