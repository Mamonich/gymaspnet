﻿using DBClasses.Contexts;
using DBClasses.Entities;
using ViewClasses.Enums;
using ViewClasses.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ASP.Net_Gym.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class AdministratorController : ControllerBase
    {
        private List<AdministratorModel> Administrators = new List<AdministratorModel>();
        private AdministratorContext administratorContext = new AdministratorContext();
        private UserContext userContext= new UserContext();

        public AdministratorController()
        {
            SetDBDataToList();
            if (Administrators.Count == 0)
            {
                List<UserEntity> users = userContext.Users.ToList();
                for (int i = 0; i < 5; i++)
                {
                    administratorContext.Add(new AdministratorEntity
                    {
                        WorkBeginTime = new TimeSpan(10 + i, 0, 0),
                        WorkEndTime = new TimeSpan(15 + i, 0, 0),
                        AdminType = i == 0 ? AdminType.MAINMANAGER : AdminType.MANAGER,
                        UserId = users[i].Id
                    });
                }
                administratorContext.SaveChanges();
                SetDBDataToList();
            }
        }

        private void SetDBDataToList()
        {
            foreach (AdministratorEntity administratorEntity in administratorContext.Administrators)
            {
                UserEntity userEntity = userContext.Users.Find(administratorEntity.UserId);
                Administrators.Add(new AdministratorModel
                {
                    Id = administratorEntity.Id,
                    Surname = userEntity.Surname,
                    Name = userEntity.Name,
                    PhoneNumber = userEntity.PhoneNumber,
                    Email = userEntity.Email,
                    Username = userEntity.Username,
                    Password = userEntity.Password,
                    Gender = userEntity.Gender,
                    BirthDate = userEntity.BirthDate.ToString(),
                    AdminType = administratorEntity.AdminType.ToString(),
                    WorkBeginTime = administratorEntity.WorkBeginTime.ToString(),
                    WorkEndTime = administratorEntity.WorkEndTime.ToString(),
                });
            }
        }

        // GET: GymController
        [HttpGet]
        public ActionResult Index()
        {       
            return Ok(Administrators);
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(Administrators.Find(g => g.Id == id));
        }

        // POST api/<ValuesController>
        [HttpPost]
        public ActionResult Post([FromBody] AdministratorModel administratorModel)
        {
            administratorContext.Add(new AdministratorEntity
            {
                AdminType        = Enum.Parse<AdminType>(administratorModel.AdminType),
                WorkBeginTime   = TimeSpan.Parse(administratorModel.WorkBeginTime),
                WorkEndTime     = TimeSpan.Parse(administratorModel.WorkEndTime),
                UserId          = administratorModel.UserId
            });
            administratorContext.SaveChanges();
            return Ok();
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] AdministratorModel administratorModel)
        {
            AdministratorEntity dbAdministratorEntity = administratorContext.Administrators.Find(id);
            dbAdministratorEntity.AdminType      = Enum.Parse<AdminType>(administratorModel.AdminType);
            dbAdministratorEntity.WorkBeginTime = TimeSpan.Parse(administratorModel.WorkBeginTime);
            dbAdministratorEntity.WorkEndTime   = TimeSpan.Parse(administratorModel.WorkEndTime);
            dbAdministratorEntity.UserId        = administratorModel.UserId;
            administratorContext.SaveChanges();
            return Ok();
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            administratorContext.Administrators.Remove(administratorContext.Administrators.Find(id));
            administratorContext.SaveChanges();
            return NoContent();
        }
    }
}
