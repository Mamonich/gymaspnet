﻿using ViewClasses.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBClasses.Entities
{
    [Table(name: "Trainers")]
    public class TrainerEntity
    {
        public int Id { get; set; }
        public TrainerType TrainerType { get; set; }
        public TrainerSpecialisation TrainerSpecialisation { get; set; }
        public TimeSpan WorkBeginTime { get; set; }
        public TimeSpan WorkEndTime { get; set; }
        public string TrainerSchedule { get; set; }


        // relations data
        //
        public ICollection<LessonEntity> Lessons { get; set; }
        public int ExerciseHallId { get; set; }
        public ExerciseHallEntity ExerciseHall { get; set; }
        public int UserId { get; set; }
        public UserEntity User { get; set; }
    }
}
