﻿using ViewClasses.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBClasses.Entities
{
    [Table(name: "Administrators")]
    public class AdministratorEntity
    {
        public int Id { get; set; }
        public AdminType AdminType { get; set; }
        public TimeSpan WorkBeginTime { get; set; }
        public TimeSpan WorkEndTime { get; set; }

        // relations data
        //
        public ExerciseHallEntity ExerciseHall { get; set; }
        public int UserId { get; set; }
        public UserEntity User{ get; set; }
    }
}
