﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBClasses.Entities
{
    [Table(name:"UserLessons")]
    [Keyless]
    [NotMapped]
    public class UserLessonsEntity
    {
        public int UserId { get; set; }
        public int LessonId { get; set; }
    }
}
