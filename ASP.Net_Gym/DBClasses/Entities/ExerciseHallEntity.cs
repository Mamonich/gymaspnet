﻿using ViewClasses.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DBClasses.Entities
{
    [Table(name: "ExerciseHalls")]
    public class ExerciseHallEntity
    {
        public int Id { get; set; }
        public TrainerSpecialisation TrainerSpecialisation { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        // relations data
        //
        public ICollection<TrainerEntity> Trainers { get; set; }
        public int GymId { get; set; }
        public GymEntity Gym { get; set; }
        public int AdministratorId { get; set; }
        public AdministratorEntity Administrator { get; set; }
    }
}
