﻿using ViewClasses.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBClasses.Entities
{
    [Table(name: "Lessons")]
    public class LessonEntity
    {
        public int Id { get; set; }
        public TimeSpan LessonBeginTime { get; set; }
        public TimeSpan LessonEndTime { get; set; }
        public LessonType LessonType { get; set; }
        public int MaxClientSize { get; set; }
        public string LessonSchedule { get; set; }

        // relations data
        //
        public int TrainerId { get; set; }
        public TrainerEntity Trainer { get; set; }        
        public ICollection<UserEntity> Users { get; set; }
    }
}
