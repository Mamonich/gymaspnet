﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBClasses.Entities;
using DBClasses.Configurations;

namespace DBClasses.Contexts
{
    public class UserLessonsContext : DbContext
    {
        public DbSet<UserLessonsEntity> UserLessons { get; set; }

        public UserLessonsContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["GymDB"].ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new UserLessonsConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }

}
