﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using DBClasses.Entities;
using DBClasses.Configurations;
using System.Linq;

namespace DBClasses.Contexts
{
    public class AdministratorContext : DbContext
    {
        public DbSet<AdministratorEntity> Administrators { get; set; }

        public AdministratorContext()
        {
            
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["GymDB"].ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AdministratorConfiguration());            
            base.OnModelCreating(modelBuilder);
        }
    }
}
