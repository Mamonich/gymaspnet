﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBClasses.Entities;
using DBClasses.Configurations;

namespace DBClasses.Contexts
{
    public class TrainerContext : DbContext
    {
        public DbSet<TrainerEntity> Trainers { get; set; }

        public TrainerContext()
        {
            Database.EnsureCreated();            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["GymDB"].ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TrainerConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
