﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using DBClasses.Entities;
using DBClasses.Configurations;

namespace DBClasses.Contexts
{
    public class GymContext : DbContext
    {
        public DbSet<GymEntity> Gyms{ get; set; }

        public GymContext()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["GymDB"].ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {               
            modelBuilder.ApplyConfiguration(new GymConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
