﻿using Microsoft.EntityFrameworkCore;
using System.Configuration;
using DBClasses.Entities;
using DBClasses.Configurations;

namespace DBClasses.Contexts
{
    public class ExerciseHallContext : DbContext
    {
        public DbSet<ExerciseHallEntity> ExerciseHalls { get; set; }

        public ExerciseHallContext()
        {
            Database.EnsureCreated();            
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           // optionsBuilder.EnableSensitiveDataLogging();
           
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["GymDB"].ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ExerciseHallConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
