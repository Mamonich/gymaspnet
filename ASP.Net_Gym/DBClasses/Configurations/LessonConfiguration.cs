﻿using DBClasses.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DBClasses.Configurations
{
    internal class LessonConfiguration : IEntityTypeConfiguration<LessonEntity>
    {
        public void Configure(EntityTypeBuilder<LessonEntity> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.Property(_ => _.LessonBeginTime);
            builder.Property(_ => _.LessonEndTime);
            builder.Property(_ => _.LessonType);
            
        }
    }
}