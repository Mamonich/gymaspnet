﻿using DBClasses.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using System.Linq;
using ViewClasses.Enums;

namespace DBClasses.Configurations
{
    internal class AdministratorConfiguration : IEntityTypeConfiguration<AdministratorEntity>
    {
        public void Configure(EntityTypeBuilder<AdministratorEntity> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.Property(_ => _.AdminType);
            builder.Property(_ => _.WorkBeginTime);
            builder.Property(_ => _.WorkEndTime);
            builder.HasOne(m => m.ExerciseHall)
                .WithOne(mh => mh.Administrator)
                .HasForeignKey<ExerciseHallEntity>(_ => _.AdministratorId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}