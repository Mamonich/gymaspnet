﻿using DBClasses.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using ViewClasses.Enums;

namespace DBClasses.Configurations
{
    public class TrainerConfiguration : IEntityTypeConfiguration<TrainerEntity>
    {
        public void Configure(EntityTypeBuilder<TrainerEntity> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.Property(_ => _.WorkBeginTime);
            builder.Property(_ => _.WorkEndTime);
        }
    }
}