﻿using DBClasses.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace DBClasses.Configurations
{
    internal class ExerciseHallConfiguration : IEntityTypeConfiguration<ExerciseHallEntity>
    {
        public void Configure(EntityTypeBuilder<ExerciseHallEntity> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.Property(_ => _.TrainerSpecialisation);
            builder.Property(_ => _.Title);
            builder.Property(_ => _.Description);
           
        }
    }
}