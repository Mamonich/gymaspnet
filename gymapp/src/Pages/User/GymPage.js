import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { Header } from '../../PagePieces/Header';
import "../../PageStyles/ListItems.css";

export function GymPage() {

  const gymId = useParams().id;
  const gymURL = "https://localhost:44395/Gym/" + gymId;
  const exerciseHallListURL = "https://localhost:44395/ExerciseHall";

  var [gym, setGym] = useState([]);
  var [ExerciseHallList, setExerciseHallList] = useState([]);
  var [ExerciseHallName, setExerciseHallName] = useState({
    title: ""
  });
  useEffect(()=>{
    fetch(gymURL)
    .then(_ => _.json() )
    .then(data => {setGym(data) });

    fetch(exerciseHallListURL)
    .then(_ => _.json() )
    .then(data => {setExerciseHallList(data)});
  }, [])

function GoToExerciseHallPage({target}){
  window.location.replace("../ExerciseHallPage/"+ target.id);
}

  return (
    <>
      <Header/>
      <body>
        <div className="Collection">    
            <div className="Item" >
                <h3>{gym.title}</h3>
                <hr/>
                <p>{gym.address}</p>
            </div>  
         <input type="text" value={ExerciseHallName.title} onChange={(e) => setExerciseHallName({...ExerciseHallName, title: e.target.value})} placeholder="input exerciseHallName, what you would like to find" />
          {ExerciseHallList.filter(h => h.gymId == gymId)
          .filter(g => g.title.includes(ExerciseHallName.title) || ExerciseHallName.title === "")
          .map(h => (
          <div className="Item" >
              <h3 id={h.id} onClick={(e) => GoToExerciseHallPage(e)}>{h.title}</h3>
              <hr/>
              <p>{h.description}
              </p>
              <hr/>
              <p>{h.trainerSpecialisation}</p>
            </div> ))}
           
        </div>
      </body>
    </>
  );
}

