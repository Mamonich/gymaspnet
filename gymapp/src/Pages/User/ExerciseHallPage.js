import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { Header } from '../../PagePieces/Header';
import "../../PageStyles/ExerciseHall.css";
import Cookies from 'js-cookie';
import { Button } from 'bootstrap';

export function ExerciseHallPage() {

  const [userId, setUserId]= useState(0);
  const lessonListURL = "https://localhost:44395/Lesson";
  const [lessonList, setLessonList] = useState([]);

  const [userLesson, setUserLesson] = useState({
    userId:'0',
    lessonId:'0'
  });

  const trainerListURL = "https://localhost:44395/Trainer";
  const [trainerList, setTrainerList] = useState([]);
  
  const exerciseHallURL = "https://localhost:44395/ExerciseHall/" + useParams().id;
  const [exerciseHall, setExerciseHall] = useState([]);

  const userLessonsURL = "https://localhost:44395/UserLessons/";
  const [userLessons, setUserLessons] = useState([]);
  
  var [TrainerSpecialisation, setTrainerSpecialisation] = useState({
    name: ""
  });

  var [Lesson, setLesson] = useState({
    trainerName: ""
  });

  useEffect(()=>{
    const UserURL = "https://localhost:44395/User/activate";

    fetch(UserURL)
    .then(_ => _.json() )
    .then(data => {setUserId(data.id)});    

    fetch(lessonListURL)
    .then(_ => _.json() )
    .then(data => {setLessonList(data) });

    fetch(trainerListURL)
    .then(_ => _.json() )
    .then(data => {setTrainerList(data) });

    fetch(exerciseHallURL)
    .then(_ => _.json() )
    .then(data => {setExerciseHall(data) });    

    fetch(userLessonsURL)
    .then(_ => _.json() )
    .then(data => {setUserLessons(data) });    

  }, [])
  
  console.log( lessonList);
  console.log( userLessons);

  const JoinOperation = ({target}) => {

     console.log(userId)
     console.log(target.id)

     fetch(userLessonsURL,{
        method:"POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({lessonId:target.id, userId:userId})
      })
  }
  const UnJoinOperation = ({target}) =>{
    fetch(userLessonsURL,{
        method:"Delete",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({lessonId:target.id, userId:userId})
      })
      .then()
      .catch()
  }
  return (
    <>
      <Header/>    
     
      <body className='Content'>
        <div className="ExerciseCollection">    
          <input type="text" value={Lesson.trainerName} 
              onChange={(e) => setLesson({...Lesson, trainerName: e.target.value})}
              placeholder="input trainerName, what you would like to find" />
          {lessonList.filter(l => l.exerciseHallId == exerciseHall.id)
          .filter(l => l.lessonType === "PUBLIC")
          .filter(l => l.trainerName.includes(Lesson.trainerName))
          .map(l =>(            
            <div className="ExerciseItem" >
                <h3>Lesson</h3>
                <hr/>
                <p>{l.lessonBeginTime} - {l.lessonEndTime}</p>
                <hr/>
                <p>{l.lessonSchedule}</p>
                <hr/>
                <p>{userLessons.filter(_ => _.lessonId == l.id).length}/{l.maxClientSize} persons joined</p>
                <hr/>
                <p>{l.trainerName}</p>
                { userLessons.filter(_ => _.lessonId == l.id).length < l.maxClientSize
                && userLessons.filter(_ => _.lessonId == l.id).filter(_ => _.userId == userId).length == 0
                  ? 
                  <form id={l.id} onSubmit={JoinOperation}>
                    <button type="submit" className="JoinBtnNotActive" id={l.id}>Join in this lesson</button>
                  </form> 
                    : 
                  <form id={l.id} onSubmit={UnJoinOperation}>
                    <button type="submit" className="JoinBtnActive" id={l.id}>
                      {userLessons.filter(_ => _.lessonId == l.id).filter(_ => _.userId == userId).length == 0 ? "No Places" : "Unjoin from this lesson"}
                    </button>
                  </form>}
                
            </div>
        ))}                
        </div>
        <div className="ExerciseCollection">
          <input type="text" value={TrainerSpecialisation.name} 
            onChange={(e) => setTrainerSpecialisation({...TrainerSpecialisation, name: e.target.value})}
            placeholder="input trainer specialisation, what you would like to find" />
          {trainerList.filter(t => t.trainerSpecialisation.includes(TrainerSpecialisation.name)).map(t => (
            <div className="Item">
              <h4>TrainerData</h4>          
              <p>Surname: {t.surname}      </p>
              <p>Name:  {t.name}        </p>
              <p>trainerSpecialisation:  {t.trainerSpecialisation}        </p>
              <p>Schedule:  {t.trainerSchedule}        </p>
              <p>PhoneNumber: {t.phoneNumber}   </p>
              <p>Email: {t.email}         </p>
              <p>Username:  {t.username}     </p>
              <p>Gender:    {t.gender}     </p>
              <p>WorkBeginTime:    {t.workBeginTime}     </p>
              <p>WorkEndTime:    {t.workEndTime}     </p>
            </div>
          ))}
        </div>
      </body>
    </>
  );
}

