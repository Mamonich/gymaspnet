import Cookies from 'js-cookie';
import { useCallback, useEffect, useState } from 'react';
import { Header } from '../../PagePieces/Header';
import "../../PageStyles/ListItems.css";

export function MainPage() {

  const GymListURL = "https://localhost:44395/Gym";

  var [GymList, setGymList] = useState([]);
  var [GymName, setGymName] = useState({
    title: ""
  });

  useEffect(()=>{
    fetch(GymListURL)
    .then(_ => _.json() )
    .then(data => {setGymList(data)});   

  }, [])
  function goToGymPage({target}){
    window.location.replace("GymPage/"+ target.id);
  }

  var fdsf = "gdf";
  return (
    <>
      <Header/>
      <body>
        <div className="Collection">    
        <input type="text" value={GymName.title} onChange={(e) => setGymName({...GymName, title: e.target.value})} placeholder="input gymName, what you would like to find"/>
        {GymList.filter(g => g.title.includes(GymName.title) || GymName.title === "").map(g => (                    
           <div className="Item" >
            <h3 id={g.id} onClick={(e)=>goToGymPage(e)}>{g.title}</h3>
            <hr/>
            <p>{g.address}
            </p>  
            </div>                    
        )
        )}
        </div>
      </body>
    </>
  );
}

