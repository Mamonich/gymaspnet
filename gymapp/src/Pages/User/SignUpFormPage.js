import { useState } from "react";
import { Form, Button } from "react-bootstrap";
import { Header } from "../../PagePieces/Header";

export function SignUpFormPage() {

    const UserURL = "https://localhost:44395/User";

    const [user, setUser] = useState({
        email:"",
        password:"",
        surname:"",	
        name:"",	
        phoneNumber:"",	
        username:"",	
        gender:"Male",        
        birthDateDay:"",
        birthDateMonth:"September",
        birthDateYear:"",        
        repeatPassword:"",
        });


    const signUp = (form)=> {

        fetch(UserURL,{
            method:"POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...user})
        })
        .then(_ => _.json())
        .then(data => setUser(data));
    };

    console.log(user);
    return(
        <>
        <Header/>
        <div style={{textAlign:"-webkit-center"}}>
            <Form className="Form" onSubmit={signUp}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                <h2>SignUp Form</h2>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" onChange={(e) => setUser({...user, email: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicUserName">
                    <Form.Label>UserName</Form.Label>
                    <Form.Control type="text" placeholder="Enter UserName" onChange={(e) => setUser({...user, username: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter Name" onChange={(e) => setUser({...user, name: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicSurname">
                    <Form.Label>Surname</Form.Label>
                    <Form.Control type="text" placeholder="Enter Surname" onChange={(e) => setUser({...user, surname: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPhoneNumber">
                    <Form.Label>PhoneNumber</Form.Label>
                    <Form.Control type="text" placeholder="Enter PhoneNumber" onChange={(e) => setUser({...user, phoneNumber: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicGender">
                    <Form.Label>Gender</Form.Label>
                    <Form.Select aria-label="Default select example" onChange={(e) => setUser({...user, gender: e.target.value})}>
                        <option>Male</option>
                        <option>Female</option>
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicBirthDate">
                    <Form.Label>BirthDate</Form.Label>
                    <Form.Control type="text" placeholder="Enter Day" onChange={(e) => setUser({...user, birthDateDay: e.target.value})}/>
                    <Form.Control  type="text" placeholder="Enter Month" onChange={(e) => setUser({...user, birthDateMonth: e.target.value})}/>
                    <Form.Control type="text" placeholder="Enter Year" onChange={(e) => setUser({...user, birthDateYear: e.target.value})}/>                    
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={(e) => setUser({...user, password: e.target.value})}/>
                </Form.Group> 
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Repeat Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={(e) => setUser({...user, repeatPassword: e.target.value})}/>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
        </>
    );
}