import Cookies from "js-cookie";
import { useState } from "react";
import { Form, Button } from "react-bootstrap";
import { Header } from "../../PagePieces/Header";
import "../../PageStyles/Form.css";

export function SignInFormPage() {

    const UserURL = "https://localhost:44395/User";

    const [user, setUser] = useState({email:"", password:""});

    const signIn = (form)=> {        
        fetch(UserURL,{
            method:"POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...user})
        })
        .then(_ => _.json())
    };

    return(
        <>
        <Header User={user}/>
        <div style={{textAlign:"-webkit-center"}}>
            <Form className="Form" onSubmit={signIn}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                <h2>SignIn Form</h2>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail" onChange={(e) => setUser({...user, email: e.target.value})}>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" onChange={(e) => setUser({...user, password: e.target.value})}/>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
        </>
    );
}