import Cookies from 'js-cookie';
import { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Header } from '../../PagePieces/Header';
import "../../PageStyles/UserCabinet.css";

export function UserCabinetPage() {

  const [cabinetUser, setCabinetUser] = useState([]);

  const UserURL = "https://localhost:44395/User/activate";
  
  useEffect(()=>{
     fetch(UserURL)
     .then(_ => _.json() )
     .then(data => {setCabinetUser(data) });
   }, [])

   function goToChangeUserPage() {
    window.location.replace("ChangeUserPage");
   }
  return (
    <>
      <Header/>       
      <div className="InfoContainer">      
        <h4>Links</h4>    
        <div>
          <Link to="/SignUpFormPage">If you haven`t account</Link>
          <Link to="/SignInFormPage">If you don`t logged in</Link>
          {cabinetUser.isAdmin > 0 && 
          <Link to="/AdminMainPage">Admin MainPage</Link>}
          
        </div>
        <h4>UserData</h4>
        <div>
        <p>Surname: {cabinetUser.surname}      </p>
        <p>Name:  {cabinetUser.name}        </p>
        <p>PhoneNumber: {cabinetUser.phoneNumber}   </p>
        <p>Email: {cabinetUser.email}         </p>
        <p>Username:  {cabinetUser.username}     </p>
        <p>Gender:    {cabinetUser.gender}     </p>
        <p>BirthDate: {cabinetUser.birthDateDay}.{cabinetUser.birthDateMonth}.{cabinetUser.birthDateYear}     </p>
        </div>
        {cabinetUser.id === undefined ? "" : <Button onClick={goToChangeUserPage}>Change UserData</Button>}
      </div>
    </>
    );
}