import Cookies from 'js-cookie';
import { useCallback, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { Header } from '../../PagePieces/Header';
import "../../PageStyles/ListItems.css";

export function AdminExerciseHallPage() {

  const userId="";
  const lessonListURL = "https://localhost:44395/Lesson/";
  const [lessonList, setLessonList] = useState([]);

  const [userLesson, setUserLesson] = useState({
    userId:'0',
    lessonId:'0'
  });

  const trainerListURL = "https://localhost:44395/Trainer/";
  const [trainerList, setTrainerList] = useState([]);
  
  const exerciseHallURL = "https://localhost:44395/ExerciseHall/" + useParams().id;
  const [exerciseHall, setExerciseHall] = useState([]);

  const userLessonsURL = "https://localhost:44395/UserLessons/";
  const [userLessons, setUserLessons] = useState([]);
  
  useEffect(()=>{
    const UserURL = "https://localhost:44395/User/activate";

    fetch(UserURL)
    .then(_ => _.json() )
    .then(data => {userId = data.id;});
    

    fetch(lessonListURL)
    .then(_ => _.json() )
    .then(data => {setLessonList(data) });

    fetch(trainerListURL)
    .then(_ => _.json() )
    .then(data => {setTrainerList(data) });

    fetch(exerciseHallURL)
    .then(_ => _.json() )
    .then(data => {setExerciseHall(data) });    

    fetch(userLessonsURL)
    .then(_ => _.json() )
    .then(data => {setUserLessons(data) });    
  }, [])


  const JoinOperation = ({target}) => {
     fetch(userLessonsURL,{
        method:"POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({lessonId:target.id, userId:userId})
      })
  }
  const UnJoinOperation = ({target}) =>{
    fetch(userLessonsURL,{
        method:"Delete",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({lessonId:target.id, userId:userId})
      })
  }

  const deleteLesson = useCallback((e) => {
    fetch(lessonListURL + e.target.id,{
      method:"DELETE"
    })
  })
  
  function goToAdminLessonItemPage({target}) {
    window.location.replace("../AdminLessonItemPage/"+target.id)
  }
  
  function goToAdminAddLessonPage() {
    window.location.replace("../AdminAddLessonPage/")
    
  }
  const deleteTrainer = useCallback((e) => {
    fetch(trainerListURL + e.target.id,{
      method:"DELETE"
    })
  })
  function goToAdminAddTrainerPage() {
    window.location.replace("../AdminAddTrainerPage/")
    
  }
  function goToAdminTrainerItemPage({target}) {
    window.location.replace("../AdminTrainerItemPage/"+target.id)
  }
  return (
    <>
      <Header/>
      <body className='Content'>
        <div className="ExerciseCollection"> 
        <button onClick={goToAdminAddLessonPage}>Add Lesson</button>   
          {lessonList.filter(l => l.exerciseHallId == exerciseHall.id)
          .map(l =>(            
            <div className="ExerciseItem" >
                <h3>Lesson</h3>
                <hr/>
                <p>{l.lessonBeginTime} - {l.lessonEndTime}</p>
                <hr/>
                <p>{l.lessonSchedule}</p>
                <hr/>
                <p>{userLessons.filter(_ => _.lessonId == l.id).length}/{l.maxClientSize} persons joined</p>
                <hr/>
                <p>{l.trainerName}</p>
                
                  {userLessons.filter(_ => _.lessonId == l.id).length < l.maxClientSize &&
                  userLessons.filter(_ => _.lessonId == l.id).filter(_ => _.userId == userId).length == 0
                  ? 
                  <form id={l.id} onSubmit={JoinOperation}>
                    <button type="submit" className="JoinBtnNotActive" id={l.id}>Join in this lesson</button>
                  </form> 
                    : 
                  <form id={l.id} onSubmit={UnJoinOperation}>
                    <button type="submit" className="JoinBtnActive" id={l.id}>
                      {userLessons.filter(_ => _.lessonId == l.id).filter(_ => _.userId == userId).length == 0 ? "No Places" : "Unjoin from this lesson"}
                    </button>
                  </form>}
              <button id={l.id} onClick={deleteLesson}>Delete</button>
              <button id={l.id} onClick={goToAdminLessonItemPage}>Change</button>
          </div>
        ))}           
        </div>
        <div className="ExerciseCollection">
        <button onClick={goToAdminAddTrainerPage}>Add Trainer</button>   
          {trainerList.map(t => (
            <div className="Item">
              <h4>TrainerData</h4>                        
              <p>Surname: {t.surname}      </p>
              <p>Name:  {t.name}        </p>
                 <p>trainerSpecialisation:  {t.trainerSpecialisation}        </p>
              <p>Schedule:  {t.trainerSchedule}        </p>
              <p>PhoneNumber: {t.phoneNumber}   </p>
              <p>Email: {t.email}         </p>
              <p>Username:  {t.username}     </p>
              <p>Gender:    {t.gender}     </p>
              <p>WorkBeginTime:    {t.workBeginTime}     </p>
              <p>WorkEndTime:    {t.workEndTime}     </p>
              <button id={t.id} onClick={deleteTrainer}>Delete</button>
              <button id={t.id} onClick={goToAdminTrainerItemPage}>Change</button>
            </div>
          ))}
        </div>
      </body>
    </>
  );
}

