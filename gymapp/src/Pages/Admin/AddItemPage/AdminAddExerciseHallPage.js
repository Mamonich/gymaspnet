import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Header } from "../../../PagePieces/Header";


export function AdminAddExerciseHallPage() {
    
    const exerciseHallURL = "https://localhost:44395/ExerciseHall";
    const [addExerciseHall, setAddExerciseHall] = useState({});    

    const gymURL = "https://localhost:44395/Gym";
    const [gymList, setGymList] = useState([]);  

    const administratorURL = "https://localhost:44395/Administrator";
    const [administratorList, setadministratorList] = useState([]);    

    const enumURL = "https://localhost:44395/Enum";
    const [enumList, setEnumList] = useState([]);  
    
    useEffect(()=>{
        fetch(enumURL)
        .then(_ => _.json())
        .then(data => setEnumList(data))

        fetch(administratorURL)
        .then(_ => _.json())
        .then(data => setadministratorList(data))

        fetch(gymURL)
        .then(_ => _.json())
        .then(data => setGymList(data))
    },[])

    const confirmAdding = (form) => {
        fetch(exerciseHallURL,{
            method:"POST",            
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...addExerciseHall})
        })
    }

    return(
        <>
            <Header/>
            <h3>AddExerciseHallPage</h3>
            <Form className="Form" onSubmit={confirmAdding}>
                <Form.Group className="mb-3" controlId="formBasicTitle">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" value={addExerciseHall.title} onChange={(e) => setAddExerciseHall({...addExerciseHall, title:e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicAddress">
                    <Form.Label>Address</Form.Label>
                    <Form.Control type="text" value={addExerciseHall.decsription} onChange={(e) => setAddExerciseHall({...addExerciseHall, decsription:e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicSpecialisation">
                    <Form.Label>Trainer`s Specialisation</Form.Label>
                    <Form.Select type="text" value={addExerciseHall.trainerSpecialisation} onChange={(e) => setAddExerciseHall({...addExerciseHall, trainerSpecialisation:e.target.value})}>
                    {enumList.map(ts =>(
                        <option >{ts}</option>
                    ))}                               
                    </Form.Select>
                </Form.Group> 
                 <Form.Group className="mb-3" controlId="formBasicSpecialisation">
                    <Form.Label>GymList</Form.Label>
                    <Form.Select type="text" value={addExerciseHall.gymId} onChange={(e) => setAddExerciseHall({...addExerciseHall, gymId:e.target.value})}>
                    {gymList.map(g =>(
                        <option >{g.id}</option>
                    ))}                               
                    </Form.Select>
                </Form.Group> 
                <Form.Group className="mb-3" controlId="formBasicSpecialisation">
                    <Form.Label>AdminList</Form.Label>
                    <Form.Select type="text" value={addExerciseHall.administratorId} onChange={(e) => setAddExerciseHall({...addExerciseHall, administratorId:e.target.value})}>
                    {administratorList.map(a =>(
                        <option >{a.id}</option>
                    ))}                                   
                    </Form.Select>
                </Form.Group>  
                <Button variant="primary" type="submit" onClick={confirmAdding}>
                    Add
                </Button>
            </Form> 
        </>
    );
}