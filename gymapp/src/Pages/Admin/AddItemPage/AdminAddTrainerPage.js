import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { Header } from "../../../PagePieces/Header";

export function AdminAddTrainerPage() {
    
    const TrainerItemURL = "https://localhost:44395/Trainer";
    const [trainer, setTrainer] = useState({});

    const userListURL = "https://localhost:44395/User/";
    const [userList, setUserList] = useState([]);    

    const exerciseHallListURL = "https://localhost:44395/ExerciseHall/";
    const [exerciseHallList, setExerciseHallList] = useState([]);
    
    const EnumURL = "https://localhost:44395/Enum";
    const [enumList, setEnumList] = useState([]);

    console.log(trainer)
    useEffect(()=>{
        
        fetch(TrainerItemURL)
        .then(_ => _.json() )
        .then(data => {setTrainer(data) });

        fetch(exerciseHallListURL)
        .then(_ => _.json() )
        .then(data => {setExerciseHallList(data) });
        
        fetch(EnumURL)
        .then(_ => _.json() )
        .then(data => {setEnumList(data) });    

        fetch(userListURL)
        .then(_ => _.json() )
        .then(data => {setUserList(data) });        
    }, [])

    const postRequest = (form)=> {        
        fetch(TrainerItemURL,{
            method:"POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...trainer})
        })
    };

    return(
        <>
        <Header/>
        <div style={{textAlign:"-webkit-center"}}>
            <Form className="Form" onSubmit={postRequest}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
            <h2>Add Trainer Form</h2>

                <Form.Label>UserId</Form.Label>
                <Form.Select value={trainer.userId} onChange={(e) => setTrainer({...trainer, userId: e.target.value})}>
                    {userList.map(u =>(
                        <option>{u.id}</option>
                    ))}
                </Form.Select>

                <Form.Label>ExerciseHallId</Form.Label>
                <Form.Select value={trainer.exerciseHallId} onChange={(e) => setTrainer({...trainer, exerciseHallId: e.target.value})}>
                    {exerciseHallList.map(h =>(
                        <option>{h.id}</option>
                    ))}
                </Form.Select>

                <Form.Label>trainerSpecialisation</Form.Label>
                <Form.Select value={trainer.trainerSpecialisation} onChange={(e) => setTrainer({...trainer, trainerSpecialisation: e.target.value})}>
                    {enumList.map(e =>(
                        <option>{e}</option>
                    ))} 
                </Form.Select>
                <Form.Label>TrainerSchedule</Form.Label>
                <Form.Control type="text" value={trainer.lessonSchedule} onChange={(e) => setTrainer({...trainer, lessonSchedule:e.target.value})}/>           
                <Form.Label>TrainerType</Form.Label>
                <Form.Select value={trainer.trainerType} onChange={(e) => setTrainer({...trainer, trainerType: e.target.value})}>
                    <option>TRAINER</option>
                    <option>MAINTRAINER</option>
                </Form.Select>

                <Form.Label>WorkBeginTime</Form.Label>
                <Form.Control type="text" placeholder="WorkBeginTime" value={trainer.workBeginTime} onChange={(e) => setTrainer({...trainer, workBeginTime: e.target.value})}/>

                <Form.Label>WorkEndTime</Form.Label>
                <Form.Control type="text" placeholder="WorkEndTime" value={trainer.workEndTime} onChange={(e) => setTrainer({...trainer, workEndTime: e.target.value})}/>
            </Form.Group> 
            <Button variant="primary" type="submit">
                Submit
            </Button>
            </Form>
        </div>
        </>
    );
}