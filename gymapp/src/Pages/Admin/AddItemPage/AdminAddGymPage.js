import { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { Header } from "../../../PagePieces/Header";


export function AdminAddGymPage() {
    
    const GymURL = "https://localhost:44395/Gym";
    const [addGymItem, setAddGymItem] = useState({});    

    const confirmAdding = (form) => {
        fetch(GymURL,{
            method:"POST",            
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...addGymItem})
        })
    }

    console.log(addGymItem);
    return(
        <>
            <Header/>
            <h3>AddGymPage</h3>
            <Form className="Form" onSubmit={confirmAdding}>
                <Form.Group className="mb-3" controlId="formBasicTitle">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" value={addGymItem.title} onChange={(e) => setAddGymItem({...addGymItem, title:e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicAddress">
                    <Form.Label>Address</Form.Label>
                    <Form.Control type="text" value={addGymItem.address} onChange={(e) => setAddGymItem({...addGymItem, address:e.target.value})}/>
                </Form.Group>
                <Button variant="primary" type="submit" onClick={confirmAdding}>
                    Add
                </Button>
            </Form> 
        </>
    );
}