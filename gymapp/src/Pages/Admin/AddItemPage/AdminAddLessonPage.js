import { useEffect, useState, useCallback } from "react";
import { Button, Form } from "react-bootstrap";
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { Header } from '../../../PagePieces/Header';


export function AdminAddLessonPage(){

    const URL_Id = useParams().id;
    const lessonURL = "https://localhost:44395/Lesson";
    const [addLesson, setAddLesson] = useState({});

    const trainerListURL = "https://localhost:44395/Trainer";
    const [trainerList, settrainerList] = useState([]);

    useEffect(()=>{
        fetch(trainerListURL)
        .then(_ => _.json())
        .then(data => {settrainerList(data)});
    },[])

    const postRequest = (form) =>{
        fetch(lessonURL,{
            method:"POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...addLesson, lessonType:"PUBLIC"})
        })
    }
console.log(addLesson)

    return(
        <>
        <Header/>
        <Form className="Form" onSubmit={postRequest}>
            <Form.Group className="mb-3" controlId="formBasicTitle">
                <Form.Label>Start Time</Form.Label>
                <Form.Control type="text" value={addLesson.lessonBeginTime} onChange={(e) => setAddLesson({...addLesson, lessonBeginTime:e.target.value})}/>
                <Form.Label>End Time</Form.Label>
                <Form.Control type="text" value={addLesson.lessonEndTime} onChange={(e) => setAddLesson({...addLesson, lessonEndTime:e.target.value})}/>
                <Form.Label>LessonSchelule</Form.Label>
                <Form.Control type="text" value={addLesson.lessonSchedule} onChange={(e) => setAddLesson({...addLesson, lessonSchedule:e.target.value})}/>           
                <Form.Label>MaxClientCount</Form.Label>
                <Form.Control type="number" value={addLesson.maxClientSize} onChange={(e) => setAddLesson({...addLesson, maxClientSize:e.target.value})}/>
                <Form.Label>TrainerList</Form.Label>
                <Form.Select type="text" value={addLesson.setAddLesson} onChange={(e) => setAddLesson({...addLesson, trainerId:e.target.value})}>
                {trainerList.map(t =>(
                    <option >{t.id}</option>
                ))}                               
                </Form.Select>
            </Form.Group>
            <Button variant="primary" type="submit">
                Add Lesson
            </Button>
        </Form> 
    </>
    );
}