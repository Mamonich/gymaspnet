import { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { Header } from '../../PagePieces/Header';
import "../../PageStyles/ListItems.css";

export function AdminGymPage() {

  const gymId = useParams().id;
  const gymURL = "https://localhost:44395/Gym/" + gymId;
  const exerciseHallListURL = "https://localhost:44395/ExerciseHall";

  var [gym, setGym] = useState([]);
  var [ExerciseHallList, setExerciseHallList] = useState([]);

  useEffect(()=>{
    fetch(gymURL)
    .then(_ => _.json() )
    .then(data => {setGym(data) });

    fetch(exerciseHallListURL)
    .then(_ => _.json() )
    .then(data => {setExerciseHallList(data)});
  }, [])  
  
  const deleteExerciseHall = useCallback((e) => {
    fetch(exerciseHallListURL,{
      method:"DELELE",
      headers:{"Content-Type":"application/json"}      
    })
  })
function goToAdminExerciseHallPage({target}){
  window.location.replace("../AdminExerciseHallPage/"+ target.id);
}
function goToAdminAddExerciseHallPage({target}){
  window.location.replace("../AdminAddExerciseHallPage/"+ target.id);
}

function goToAdminExerciseHallItemPage({target}) {
  window.location.replace("../AdminExerciseHallItemPage/"+ target.id);
}
  return (
    <>
      <Header/>
      <body>
      <button onClick={goToAdminAddExerciseHallPage}>Add ExerciseHall</button>

        <div className="Collection">    
            <div className="Item" >
                <h3>GymName</h3>
                <hr/>
                <p>Address
                </p>
            </div>  
            {ExerciseHallList.map(eh=>(
               <div className="Item" >
               <h3 id={eh.id} onClick={(e) => goToAdminExerciseHallPage(e)}>{eh.title}</h3>
               <hr/>
               <p>{eh.description}
               </p>
               <hr/>
               <p>{eh.trainerSpecialisation}</p>
               <button id={eh.id} onClick={deleteExerciseHall}>Delete</button>
               <button id={eh.id} onClick={goToAdminExerciseHallItemPage}>Change</button>
             </div>  
            ))}
        </div>
      </body>
    </>
  );
}

