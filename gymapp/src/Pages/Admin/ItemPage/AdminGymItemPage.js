import { useEffect, useState, useCallback } from "react";
import { Button, Form } from "react-bootstrap";
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { Header } from '../../../PagePieces/Header';


export function AdminGymItemPage(){

    const URL_Id = useParams().id;
    const GymItemURL = "https://localhost:44395/Gym/"+URL_Id;

    const [changeGymItem, setChangeGymItem] = useState({});

    useEffect(()=>{
        fetch(GymItemURL)
        .then(_ => _.json() )
        .then(data => {setChangeGymItem(data)});
    }, [])

    console.log(changeGymItem);

    const confirmChanges = (form) =>{
        fetch(GymItemURL,{
            method:"PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...changeGymItem})
        })
    }

    return(
        <>
        <Header/>
        <Form className="Form" onSubmit={confirmChanges}>
            <Form.Group className="mb-3" controlId="formBasicTitle">
                <Form.Label>Title</Form.Label>
                <Form.Control type="text" value={changeGymItem.title} onChange={(e) => setChangeGymItem({...changeGymItem, title:e.target.value})}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicAddress">
                <Form.Label>Address</Form.Label>
                <Form.Control type="text" value={changeGymItem.address} onChange={(e) => setChangeGymItem({...changeGymItem, address:e.target.value})}/>
            </Form.Group>
            <Button variant="primary" type="submit">
                Confirm Changes
            </Button>
        </Form> 
    </>
    );
}