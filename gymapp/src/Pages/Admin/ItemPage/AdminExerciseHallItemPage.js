import { useEffect, useState, useCallback } from "react";
import { Button, Form } from "react-bootstrap";
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { Header } from '../../../PagePieces/Header';


export function AdminExerciseHallItemPage(){

    const URL_Id = useParams().id;
    const ExerciseHallItemURL = "https://localhost:44395/ExerciseHall/"+URL_Id;
    const EnumURL = "https://localhost:44395/Enum/";

    const [changeExerciseHallItem, setChangeExerciseHallItem] = useState({});
    const [list, setList] = useState([]);

    useEffect( ()=>{
       fetch(ExerciseHallItemURL)
        .then(_ => _.json() )
        .then(data => {setChangeExerciseHallItem(data)})
       fetch(EnumURL)
        .then(_ => _.json() )
        .then(data => {setList(data)})
    }, [])

    console.log(changeExerciseHallItem);
    console.log(list);

    const confirmChanges = async (form) => {
        await fetch(ExerciseHallItemURL,{
            method:"PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...changeExerciseHallItem})
        })
    }

    return(
        <>
        <Header/>
        <Form className="Form" onSubmit={confirmChanges}>
            <Form.Group className="mb-3" controlId="formBasicTitle">
                <Form.Label>Title</Form.Label>
                <Form.Control type="text" value={changeExerciseHallItem.title} onChange={(e) => setChangeExerciseHallItem({...changeExerciseHallItem, title:e.target.value})}/>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control type="text" value={changeExerciseHallItem.description} onChange={(e) => setChangeExerciseHallItem({...changeExerciseHallItem, description:e.target.value})}/>
            </Form.Group>
             <Form.Group className="mb-3" controlId="formBasicSpecialisation">
                <Form.Label>Trainer`s Specialisation</Form.Label>
                <Form.Select type="text" value={changeExerciseHallItem.trainerSpecialisation} onChange={(e) => setChangeExerciseHallItem({...changeExerciseHallItem, trainerSpecialisation:e.target.value})}>
                 {list.map(ts =>(
                     <option >{ts}</option>
                 ))}                               
                </Form.Select>
            </Form.Group> 
            <Button variant="primary" type="submit">
                Confirm Changes
            </Button>
        </Form> 
    </>
    );
}