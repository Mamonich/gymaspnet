import { Alert } from "bootstrap";
import { useEffect, useState, useCallback } from "react";
import { Button, Form } from "react-bootstrap";
import { useParams } from 'react-router-dom/cjs/react-router-dom.min';
import { Header } from '../../../PagePieces/Header';


export function AdminLessonItemPage(){

    const URL_Id = useParams().id;
    const LessonItemURL = "https://localhost:44395/Lesson/"+URL_Id;

    const [changeLessonItem, setChangeLessonItem] = useState({});

    useEffect(()=>{
        fetch(LessonItemURL)
        .then(_ => _.json() )
        .then(data => {setChangeLessonItem(data)});
    }, [])


    const confirmChanges = (form) =>{    
        fetch(LessonItemURL,{
            method:"PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...changeLessonItem})
        })
    }

    console.log(changeLessonItem);
    return(
        <>
        <Header/>
        <Form className="Form" onSubmit={confirmChanges}>
            <Form.Group className="mb-3" controlId="formBasicTitle">
                <Form.Label>Start Time</Form.Label>
                <Form.Control type="text" value={changeLessonItem.lessonBeginTime} onChange={(e) => setChangeLessonItem({...changeLessonItem, lessonBeginTime:e.target.value})}/>
                <Form.Label>End Time</Form.Label>
                <Form.Control type="text" value={changeLessonItem.lessonEndTime} onChange={(e) => setChangeLessonItem({...changeLessonItem, lessonEndTime:e.target.value})}/>
                <Form.Label>LessonSchelule</Form.Label>
                <Form.Control type="text" value={changeLessonItem.lessonSchedule} onChange={(e) => setChangeLessonItem({...changeLessonItem, lessonSchedule:e.target.value})}/>           
                <Form.Label>MaxClientCount</Form.Label>
                <Form.Control type="number" value={changeLessonItem.maxClientSize} onChange={(e) => setChangeLessonItem({...changeLessonItem, maxClientSize:e.target.value})}/>
            </Form.Group>
            <Button variant="primary" type="submit">
                Confirm Changes
            </Button>
        </Form> 
    </>
    );
}