import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { Header } from "../../../PagePieces/Header";

export function AdminTrainerItemPage() {
    const URL_Id = useParams().id;

    const UserURL = "https://localhost:44395/User/";
    const [user, setUser] = useState({});

    const TrainerItemURL = "https://localhost:44395/Trainer/"+ URL_Id;
    const [trainer, setTrainer] = useState({});
    
    const EnumURL = "https://localhost:44395/Enum";
    const [enumList, setEnumList] = useState([]);
    console.log(trainer)
    useEffect(()=>{
        
        fetch(TrainerItemURL)
        .then(_ => _.json() )
        .then(data => {setTrainer(data) });
        
        fetch(EnumURL)
        .then(_ => _.json() )
        .then(data => {setEnumList(data) });

        fetch(UserURL + trainer.userId)
        .then(_ => _.json() )
        .then(data => {setUser(data) });
    }, [])

    const putRequest = (form)=> {

        fetch(UserURL + trainer.userId,{
            method:"PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...user})
        })
        fetch(TrainerItemURL,{
            method:"PUT",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({...trainer})
        })
    };

    return(
        <>
        <Header/>
        <div style={{textAlign:"-webkit-center"}}>
            <Form className="Form" onSubmit={putRequest}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                <h2>Change Trainer Form</h2>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={user.email} onChange={(e) => setUser({...user, email: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicUserName">
                    <Form.Label>UserName</Form.Label>
                    <Form.Control type="text" placeholder="Enter UserName" value={user.username} onChange={(e) => setUser({...user, username: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter Name" value={user.name} onChange={(e) => setUser({...user, name: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicSurname">
                    <Form.Label>Surname</Form.Label>
                    <Form.Control type="text" placeholder="Enter Surname" value={user.surname} onChange={(e) => setUser({...user, surname: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPhoneNumber">
                    <Form.Label>PhoneNumber</Form.Label>
                    <Form.Control type="text" placeholder="Enter PhoneNumber" value={user.phoneNumber} onChange={(e) => setUser({...user, phoneNumber: e.target.value})}/>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicGender">
                    <Form.Label>Gender</Form.Label>
                    <Form.Select aria-label="Default select example" value={user.gender} onChange={(e) => setUser({...user, gender: e.target.value})}>
                        <option>Male</option>
                        <option>Female</option>
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicBirthDate">
                    <Form.Label>BirthDate</Form.Label>
                    <Form.Control type="text" placeholder="Enter Day" value={user.birthDateDay} onChange={(e) => setUser({...user, birthDateDay: e.target.value})}/>
                    <Form.Control type="text" placeholder="Enter Month" value={user.birthDateMonth} onChange={(e) => setUser({...user, birthDateMonth: e.target.value})}/>
                    <Form.Control type="text" placeholder="Enter Year" value={user.birthDateYear} onChange={(e) => setUser({...user, birthDateYear: e.target.value})}/>                    
                </Form.Group>
                <Form.Label>TrainerSchedule</Form.Label>
                <Form.Control type="text" value={user.lessonSchedule} onChange={(e) => setUser({...user, lessonSchedule:e.target.value})}/>           
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>trainerSpecialisation</Form.Label>
                    <Form.Select value={trainer.trainerSpecialisation} onChange={(e) => setTrainer({...trainer, trainerSpecialisation: e.target.value})}>
                        {enumList.map(e =>(
                            <option>{e}</option>
                        ))}
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>TrainerType</Form.Label>
                    <Form.Select value={trainer.trainerType} onChange={(e) => setTrainer({...trainer, trainerType: e.target.value})}>
                        <option>TRAINER</option>
                        <option>MAINTRAINER</option>
                    </Form.Select>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>WorkBeginTime</Form.Label>
                    <Form.Control type="text" placeholder="WorkBeginTime" value={trainer.workBeginTime} onChange={(e) => setTrainer({...trainer, workBeginTime: e.target.value})}/>
                </Form.Group> 
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>WorkEndTime</Form.Label>
                    <Form.Control type="text" placeholder="WorkEndTime" value={trainer.workEndTime} onChange={(e) => setTrainer({...trainer, workEndTime: e.target.value})}/>
                </Form.Group> 
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="text" placeholder="Password" value={user.password} onChange={(e) => setUser({...user, password: e.target.value})}/>
                </Form.Group> 
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
        </>
    );
}