import { useCallback, useEffect, useState } from 'react';
import { Header } from '../../PagePieces/Header';
import "../../PageStyles/ListItems.css";

export function AdminMainPage() {

  const GymListURL = "https://localhost:44395/Gym/";

  var [GymList, setGymList] = useState([]);

  useEffect(()=>{
    fetch(GymListURL)
    .then(_ => _.json() )
    .then(data => {setGymList(data) });

  }, [])

  function goToAdminGymPage({target}){
    window.location.replace("AdminGymPage/"+ target.id);
  }

  function goToAdminAddGymPage(){
    window.location.replace("AdminAddGymPage");
  }

  const deleteGym = useCallback((e) => {
    fetch(GymListURL + e.target.id,{method:"DELETE"})
  })

  const changeGym = useCallback((e) => {
    console.log(e.target.id);
    window.location.replace("AdminGymItemPage/"+ e.target.id);
  })

  return (
    <>
      <Header/>
      <body>
        
        
          <button onClick={goToAdminAddGymPage}>Add Gym</button>
        <div className="Collection">
        {GymList.map(g => (
          <div className="Item" >
          <h3 id={g.id} onClick={(e)=>goToAdminGymPage(e)}>{g.title}</h3>
          <hr/>
          <p>{g.address}
          </p>
          <button id={g.id} onClick={deleteGym}>Delete</button>
          <button id={g.id} onClick={changeGym}>Change</button>
        </div>  
        ))}
        </div>
      </body>
    </>
  );
}

