import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { AdminAddExerciseHallPage } from './Pages/Admin/AddItemPage/AdminAddExerciseHallPage';
import { AdminAddGymPage } from './Pages/Admin/AddItemPage/AdminAddGymPage';
import { AdminAddLessonPage } from './Pages/Admin/AddItemPage/AdminAddLessonPage';
import { AdminAddTrainerPage } from './Pages/Admin/AddItemPage/AdminAddTrainerPage';
import { AdminExerciseHallPage } from './Pages/Admin/AdminExerciseHallPage';
import { AdminGymPage } from './Pages/Admin/AdminGymPage';
import { AdminMainPage } from './Pages/Admin/AdminMainPage';
import { AdminExerciseHallItemPage } from './Pages/Admin/ItemPage/AdminExerciseHallItemPage';
import { AdminGymItemPage } from './Pages/Admin/ItemPage/AdminGymItemPage';
import { AdminLessonItemPage } from './Pages/Admin/ItemPage/AdminLessonItemPage';
import { AdminTrainerItemPage } from './Pages/Admin/ItemPage/AdminTrainerItemPage';
import { ChangeUserPage } from './Pages/User/ChangeUserPage';
import { ExerciseHallPage } from './Pages/User/ExerciseHallPage';
import { GymPage } from "./Pages/User/GymPage";
import { MainPage } from "./Pages/User/MainPage";
import { SignInFormPage } from './Pages/User/SignInFormPage';
import { SignUpFormPage } from './Pages/User/SignUpFormPage';
import { UserCabinetPage } from './Pages/User/UserCabinetPage';

export function RoutesApp() {
    return(        
        <Router>
            <Switch>
                <Route exact path="/" component={MainPage}/>
                <Route path="/GymPage/:id" component={GymPage}/>
                <Route path="/UserCabinetPage" component={UserCabinetPage}/>
                <Route path="/ChangeUserPage" component={ChangeUserPage}/>
                <Route path="/ExerciseHallPage/:id" component={ExerciseHallPage}/>
                <Route path="/SignUpFormPage" component={SignUpFormPage}/>
                <Route path="/SignInFormPage" component={SignInFormPage}/>
                <Route path="/AdminMainPage" component={AdminMainPage}/>
                <Route path="/AdminGymPage/:id" component={AdminGymPage}/>
                <Route path="/AdminExerciseHallPage/:id" component={AdminExerciseHallPage}/>
                <Route path="/AdminExerciseHallItemPage/:id" component={AdminExerciseHallItemPage}/>
                <Route path="/AdminGymItemPage/:id" component={AdminGymItemPage}/>
                <Route path="/AdminLessonItemPage/:id" component={AdminLessonItemPage}/>
                <Route path="/AdminTrainerItemPage/:id" component={AdminTrainerItemPage}/>
                <Route path="/AdminAddGymPage/" component={AdminAddGymPage}/>
                <Route path="/AdminAddExerciseHallPage/" component={AdminAddExerciseHallPage}/>
                <Route path="/AdminAddLessonPage/" component={AdminAddLessonPage}/>                
                <Route path="/AdminAddTrainerPage/" component={AdminAddTrainerPage}/>                
            </Switch>
        </Router>

    );
}

