import '../PageStyles/Header.css'
import { UserCabinet } from './UserCabinet';
import "bootstrap";

export function Header(){
    
    
    function GoToMainPage() {
        window.location.replace("/");
    }

    return(
        <header>
            <div>
                <span onClick={GoToMainPage}>MainMenu</span>
            </div>
            <div>
                <UserCabinet/>
            </div>
        </header>
    );
}