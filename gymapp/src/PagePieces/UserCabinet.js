import Cookies from "js-cookie";
import { useEffect, useState } from "react";


export function UserCabinet(){

    const [user, setUser] = useState({

    });

    const UserURL = "https://localhost:44395/User/activate";

    useEffect(() => {
        fetch(UserURL)
        .then(_ => _.json() )
        .then(data => {setUser(data) });
    },[]);
    function GoToUserCabinetPage() {
        window.location.replace("/UserCabinetPage");
    }
    return(
        <>
            <i class="bi bi-person-circle">{user.username ? user.username : "Guest"}</i>
            <br/>
            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16" onClick={GoToUserCabinetPage}>
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
            </svg>
        </>
    );
}