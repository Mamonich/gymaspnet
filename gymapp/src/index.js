import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import { RoutesApp } from './RoutesApp';
import 'bootstrap/dist/css/bootstrap.min.css';

document.body.style="background:rgb(31, 255, 1);"

ReactDOM.render(
  <React.StrictMode>
    <RoutesApp />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
